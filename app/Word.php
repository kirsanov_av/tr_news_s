<?php
/**
 * Created by PhpStorm.
 * User: kirsanov_av
 * Date: 19.05.2019
 * Time: 14:55
 */
namespace NewsUpload;

use PhpOffice\PhpWord\Element\Table;
use \PhpOffice\PhpWord\Element\TextRun;
use \PhpOffice\PhpWord\Element\Text;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

class Word
{

    public $var = 'Переменная';
    public $path;
    public $image;
    public $image_type;
    public $files = array();
    private $countImg = 0;
    private $text = array();



    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }



    /**
     * Получаем вордовые документы
     * @return bool
     */
    private function getWordDocument(){
        $path = $this->getPath();

        return true;
    }

    public function readWordDocument(){
        $path = $this->path;
        if (!isset($path)){
            return false;
        }
        $i=0;
        $wordDocs = array();
        foreach (glob($path."*.[dD][oO][cC]") as $word) {
            $wordDocs [] = $word;
        }
        foreach (glob($path."*.[dD][oO][cC][xX]") as $word) {
            $wordDocs [] = $word;
        }

        if (is_array($wordDocs) && array_key_exists(0,$wordDocs))
        {
            $wordDoc = $wordDocs[0];
        }
        else{
            echo "Нет текстового документа для разбора, процесс прерван";
            die;
        }

        //$word = new \PhpOffice\PhpWord\PhpWord();
        $obj = IOFactory::createReader('Word2007');
        $word = $obj->load($wordDoc);
        $sections = array();
        $text = array();
        foreach ($word->getSections() as $section){
            $sections[] = $section;
            $arrsec = $section->getElements();

            foreach ($arrsec as $elements) {
                if ($elements instanceof TextRun) {
                    $body = '';
                    foreach ($elements->getElements() as $element){
                        if ($element instanceof Text) {
                            $body .= $element->getText();
                        }
                    }
                    $this->text[]= array(
                        'string' => $body,
                        'count' => mb_strlen($body,'utf-8'),
                        );
                }
            }
        }

        return $this->text;

    }

    public function mergeText(){

    }

}