<?php
/**
 * Created by PhpStorm.
 * User: kirsanov_av
 * Date: 05.04.2019
 * Time: 10:18
 */
namespace NewsUpload;
/*include_once "Image.php";
include_once "Word.php";
include_once "SFTP.php";
include_once "NewsTable.php";*/

//require_once "../vendor/autoload.php";
class News extends Run
{
    public $year;
    public $folderNews; //Полный путь до файла
    public $folderImg; //
    public $post;
    public $files = array();
    public $countImg = 0;
    public $countImgLeftRight = 6;
    public $nameNews = '';

    public $carouselString = '';
    public $carouselArray = array();

    public $serverPathImage = "/tambov-rosnou.ru/public_html/images/images_all/news/";


    function exec() {
        //TODO Автоматически получать директорию для размещения новости
        //$nameDirNews = "2019-04-03";
        //$nameDirNews = $this->getDateFromPath($path);
        echo ("Начинаем обработку данных.<br>");
        ob_flush();
        ob_start();
        if ($this->formLoad($_POST)){
            //1. Указать путь до папки с файлами

            $image = new Image();
            $image->setPath($this->folderNews);
            $word = new Word();
            $word->setPath($this->folderNews);

            echo ("Узнали путь, где храняться файлы.<br>");
            ob_flush();
            ob_start();
            //1.5 Получаем данные из ворд-документа
            $textWord = $word->readWordDocument();
            /*var_dump($textWord);
            die;*/

            //2. Получить список всех картинок в этой папке
            if (!$this->files = $image->preparingImage()){
                //Ошибка при подготовке картинок
            }
            echo ("Получили все картинки из каталога.<br>");
            ob_flush();
            ob_start();
            //3.Загружаем все файлы на сервер
            $this->putFilesToFtp();
            echo ("Обработали и загрузили все картинки на сервер.<br>");
            ob_flush();
            ob_start();
            //4. Генерируем HTML для карусели сайта на эти файлы
            $imgHtml = $this->genHtmlImageCarousel();
            echo ("Сгенерировали HTML-код для вставки картинок в новость.<br>");
            ob_flush();
            ob_start();



            //5. Открываем Word-документ, который лежит в той же папке, что и фотки
            //и получаем из него текст

            //6. Преобразоваываем текст в HTML и объединяем с фотками
            //Правило объединения - есть К количество слов, после каждого К-го слова мы ищем закрывающийся тег </p>
            //И после него вставляем очередную группу(слева-справа) картинок.
            // Если кончился весь текст и кончились группировки картинок (слева-справа),
            //то переходим в конец текста, и вставляем остальные картинки, как получится.
            echo ("Получаем форматированный для новости текст и картинки.<br>");
            ob_flush();
            ob_start();
            $imgCarousel = $this->genHtmlImageCarousel(true,true);
            $textNews = $this->mergeTextFromImage($textWord,$imgCarousel);


            //7. Подключаемся к базе данных сайта и создаем в необходимой таблицы новую строчку с данной новостью,
            //а затем вставляем туда готовый текст и заполняем остальные атрибуты данной таблицы
            echo ("Сохраняем все данные в БД.<br>");
            ob_flush();
            ob_start();
            $news = new NewsTable();
            $news->title = $this->nameNews;
            $news->introtext = $textNews['intro'];
            $news->fulltext = $textNews['full'];

            $news->alias = $this->folderImg . '-' . date('H-i-s');
            if (array_key_exists($this->getDateFromPath($this->folderImg,'m'), $news->monthCategId)){
                $news->catid = $news->monthCategId[$this->getDateFromPath($this->folderImg,'m')];
            }
            else{
                $news->catid = 44;
            }
            date_default_timezone_set('UTC');
            $news->created = $news->publish_up = date("Y-m-d H:i:s");

            //8. Помещаем на главную страницу новость
            $res = $news->insertNews(); //Пока не работает

            echo ("Создали запись о новой новости - $res <br><hr><p>&nbsp;</p>");
            ob_flush();
            echo $imgHtml;

            //...Удаляем локальные файлы
            echo "<p>&nbsp;</p>";
            echo "<hr />";

            echo $this->printForm();

        }
        else{
            echo $this->printForm();
        }

    }

    /**
     * $html - признак вывода данных - либо в формате HTML(true), либо с "безопасными"(false) символами
     * @return string|array
     */
    public function genHtmlImageCarousel($html = false, $typeOut = 'text'){
        $stop = $this->countImgLeftRight;
        $pathUrl = "/images/images_all/news/$this->year/$this->folderImg";
        $res = '';
        $second = false;
        $resArray = array();
        //Цикл по количеству файлов
        for ($i = 1; $i <= $this->countImg; $i++) {
            $tmp = '';
            if ($i > $stop) {
                $align = 'left';
                $second = true;
            } else {
                if ($second) {
                    $second = false;
                    $align = 'right';
                } else {
                    $second = true;
                    $align = 'left';
                }
            }
            if ($second && $i > 2 && $i <= $stop+1) {
                $tmp .= $html ? addslashes('<div style="clear:both;"></div>') : "<p>&nbsp;</p>" . htmlspecialchars('<div style="clear:both;"></div>');

            }
            elseif($i == 3){
                $tmp .= "<p>&nbsp;</p>";
            }
            $tmp .=
                $html ?
                    addslashes($this->templImgGenerate($pathUrl , $i, 'jpg', $this->nameNews, $align)) :
                htmlspecialchars($this->templImgGenerate($pathUrl , $i, 'jpg', $this->nameNews, $align)) . "<br>";
            $res .= $tmp;
            $resArray[] = $tmp;
        }
        $this->carouselArray = $resArray;
        if ($typeOut == 'array'){
            return $resArray;
        }
        else{
            return $res;
        }

    }

    /**
     * Загружаем все данные из формы, начать обработку новости
     * @param $post
     * @return bool
     */
    public function formLoad($post){
        /**
         * Получили данные от файла, обрабатываем все.
         */
        if (array_key_exists('folder_img', $post) && array_key_exists('count_img', $post)) {
            $this->countImg = (int)$post['count_img'];
            //формируем константы для пути к папкам
            if (array_key_exists('stopper', $post)) {
                $this->countImgLeftRight = intval($post['stopper']);
            } else {
                $this->countImgLeftRight = 6;
            }

            if (array_key_exists('name_news', $post) && $post['name_news'] != '') {
                $this->nameNews = $post['name_news'];
            } else {
                $this->nameNews = 'Фото';
            }
            $this->folderNews = $this->path($post['folder_img']);
            $this->year = $this->getDateFromPath($this->folderNews, 'Y');
            $this->folderImg = $this->getDateFromPath($this->folderNews);

            return true;
        }
        return false;
    }

    private function templImgGenerate($fullPathImg, $name, $extension, $nameNews = '', $align = 'left')
    {
        if ($align !== 'left' && $align !== 'right') {
            $align = 'left';
        }
        if ($nameNews == '') {
            $nameNews = "Фото";
        }
        $str = '<p>
        <a href="' . $fullPathImg . '/' . $name . '.' . $extension . '" rel="lytebox[1]" title="' . $nameNews . '">
            <img alt="'. $nameNews . '" src="' . $fullPathImg . '/' . $name . 's.' . $extension . '" style="float:' . $align . ';" />
        </a>
    </p>
    ';
        return $str;
    }

    public function printForm(){
        return <<<FORM
    <form action="" method="post">
        <div>
            <label>Полный путь до каталога фотографий (В конце должен стоять знак "/". Название каталога должно быть с указанием даты, например "2018-01-01 Имя новости") * </label><br>
            <input type="text" name="folder_img" style="width: 90%;"/>
        </div>
        <div>
            <label>Количество изображений *</label><br>
            <input type="text" name="count_img"  style="width: 90%;"/>
        </div>
        <div>
            <label>Количество изображений, размещаемых по обоим краям (по умолчанию 6, остальные размещаются по левому
                краю)</label><br>
            <input type="text" name="stopper" value="6" style="width: 90%;"/>
        </div>
        <div>
            <label>Название новости/картинок к ней</label><br>
            <input type="text" name="name_news" value="Фото" style="width: 90%;"/>
        </div>
        
        <input type="submit">
    </form>
FORM;
    }

    function closeFile(){
        //Закрыть выбранный файл
    }

    function deleteFile(){
        //Удалить выбранный файл
    }

    function putFilesToFtp()
    {
        $ftp = new SFTP("vh432.timeweb.ru", "cp68727_ftp", "T6sBbWpz");
        $year = $this->year; //= date("Y",strtotime($nameDir));
        $remotePath = $this->serverPathImage.$year."/";
        $nameDir = $this->folderImg;
        $ftp->passive =true;
        if ($ftp->connect()) {
            //переходим в нужную директорию на сервере
            if ($ftp->cd($remotePath)){
                $lists = $ftp->ls();
                $stop = false;
                //Проверку на наличие уже существующей директории
                $nameDir = $this->filesExists($nameDir, $lists);
                /*foreach ($lists as $list){
                    if ($list == $nameDir){
                        $stop = true;
                    }
                }
                if ($stop) {
                    $tempName = $nameDir;
                    //Меняем имя исходящей папки

                    $nameDir = $tempName . '-2';
                    $stop = false;
                }*/
//                if(!$stop){
                    if (!$ftp->mkdir($nameDir)){
                        return "Ошибка создания директории";
                    }
                    else{
                        $ftp->cd($nameDir);
                        $remotePath .= $nameDir . "/";
                        $this->folderImg = $nameDir;
                    }
//                }
            }

            //начинаем загружать все файлы в свои папки
            foreach ($this->files as $file){
                $localFile = $file['path'] . $file['name'];
                $remoteFile = $remotePath . $file['name'];
                $ftp->put($localFile,$remoteFile,FTP_BINARY);
            }
        }
    }


    /**
     * Функция проверяет существование директории/файла и если она/он существует, то дает новое автоматическое имя
     * Возвращает имя для этого файла/каталога
     * @param string $name - имя проверяемого файла
     * @param array $fileLists - список файлов и директорий в проверяемой директории (список из команды ls)
     * @return string
     */
    private function filesExists(string $name, array $fileLists = array()){
        $tempName = $name;
        $stop = false;
        //Меняем имя исходящей папки
        foreach ($fileLists as $list){
            if ($list == $tempName){
                $stop = true;
                break;
            }
        }

        $do = false;

        if ($stop == true){
            $match = array();
            preg_match('/(\d{4}-\d{2}-\d{2})-([0-9]*)/',$tempName,$match);

            if (is_array($match) && count($match) > 2){
                $index = (int)$match[2];
                $main = $match[1];
                $do = true;
            }
            else{
                $index = 1;
            }
            if ($do == true && $stop == true) {
                $index++;
                $tempName = $main.'-'.$index;
            }
            elseif($stop == true){
                $tempName .= '-1';
            }
            $tempName = $this->filesExists($tempName,$fileLists);
        }
        return $tempName;
    }

    private function getDateFromPath($p, $format = "Y-m-d")
    {
        $p=str_replace('\\','/',trim($p));
        if (substr($p,-1)=='/')
            $p=substr($p,0,-1);
        $a=explode('/', $p);
        $lastDir = array_pop($a);
        if(preg_match("/\d{4}(-|\/|_|.)\d{2}(-\/|_|.)\d{2}/", $lastDir, $match)) {
            return date($format, strtotime($match[0]));
        }
        else{
            return date($format);
        }
    }

    private function path($path){
        if (is_string($path) && strpos($path,'/') !== FALSE && strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
            $slash = array("/" => "\\");
            $res = strtr($path, $slash);
            if (strpos(trim($res),'\\',-1) === false){
                $res .= '\\';
            }
            return $res;
        }
        return $path;
    }

    /**
     * Собирает текст и размещает его вместе с картинками
     * @param $arrayText
     * @param $arrayImage
     * @return array
     */
    public function mergeTextFromImage($arrayText, $arrayImage){
        $count = 0; //Количество символов
        $countLeftRight = 480; //Шаблонное количество символов, которое необходимо для картинок LeftRight
        $countImgIn = 0; //Количество картинок, которые уже помещены в текст
        $countImg = $this->countImg;
        $bIntro = false;
        $intro = '';
        $full = '';
        foreach ($arrayText as $text){
            $tmp = '';

            //Добавляем картинки
            if ($count == 0 && $countImgIn < $this->countImgLeftRight){
                $tmp .= $arrayImage[$countImgIn];
                $countImgIn++;
                $tmp .= $arrayImage[$countImgIn];
                $countImgIn++;
            }
            $count += $text['count'];
            //Добавляем текст
            $tmp .= "<p>";
            $tmp .= addslashes($text['string']);
            $tmp .= "</p>";
            $full .= $tmp;

            if ($count >= $countLeftRight){
                $count = 0;
                if ($bIntro === false){
                    $intro = $full;
                    $full = '';
                    $bIntro = true;
                }
            }

        }

        //Добавляем остальные картинки вниз новости
        while ($countImg > $countImgIn){
            $full .= $arrayImage[$countImgIn];
            $countImgIn++;
        }

        $res = array(
            'intro' => $intro,
            'full' => $full,
        );
        return $res;
    }
}