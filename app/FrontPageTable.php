<?php
namespace NewsUpload;
class FrontPageTable extends RosnouMysql
{
    public $ordering = 0;
    public $content_id = 0;

    public function getTableName(){
        return "ros_content_frontpage";
    }

    private function updateOrdering(){
        $sql = "UPDATE ". $this->getTableName() .
            "SET ordering = ordering+1".
            "WHERE 1=1";
        //$query = new RosnouMysql();
        $this->sql = $sql;
        if ($result = $this->exec()){
            return true;
        }
        return false;
    }

    public function save(){

    }
}