<?php
/**
 * Created by PhpStorm.
 * User: kirsanov_av
 * Date: 20.03.2019
 * Time: 9:11
 */
namespace NewsUpload;

//include "SFTP.php";
use app\SFTP;
use app\Run;
//require_once "../vendor/autoload.php";
class Image
{
    public $var = 'Переменная';
    public $path;
    public $image;
    public $image_type;
    public $files = array();
    private $countImg = 0;



    const SMALL_IMG = 120;
    const NORMAL_IMG = 1200;
    const BIG_IMG = 1600;



    public function displayVar () {
        echo $this->getVar();
    }

    /**
     * @param string $var
     * @return ImageClass
     */
    public function setVar($var)
    {
        $this->var = $var;
        return $this;
    }

    /**
     * @return string
     */
    public function getVar()
    {
        return $this->var;
    }

    public function preparingImage(){
        $path = $this->path;
        if (!isset($path)){
            return false;
        }
        $i=0;
        foreach (glob($path."*.[jJ][pP][gG]") as $image){
            $i++;
            $this->load($image);

            $height = $this->getHeight();
            $width = $this->getWidth();
            if($height >= $width){
                $this->resizeToHeight(self::NORMAL_IMG);
            }
            else{
                $this->resizeToWidth(self::NORMAL_IMG);
            }
            //$this->resize(1200, 800);
            $this->orientation($image);
            $name = $i.'.jpg';
            $file = $path.$name;
            $this->save($file);
            $this->files[] =
                array(
                    'path' => $path,
                    'name' => $name,
                );
            $height = $this->getHeight();
            $width = $this->getWidth();
            if($height >= $width){
                $this->resizeToHeight(self::SMALL_IMG);
            }
            else{
                $this->resizeToWidth(self::SMALL_IMG);
            }
            $name = $i.'s.jpg';
            $file = $path.$name;
            $this->save($file);
            $this->files[] =
                array(
                    'path' => $path,
                    'name' => $name,
                );
        }
        $this->countImg = $i;
        return $this->files;
    }
    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    function load($filename) {
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        } elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }
    function save($filename, $image_type=IMAGETYPE_JPEG, $compression=90, $permissions=null) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image,$filename);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->image,$filename);
        }
        if( $permissions != null) {
            chmod($filename,$permissions);
        }
    }

    function getWidth() {
        return imagesx($this->image);
    }
    function getHeight() {
        return imagesy($this->image);
    }
    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }
    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width,$height);
    }
    function scale($scale) {
        $width = $this->getWidth() * $scale/100;
        $height = $this->getheight() * $scale/100;
        $this->resize($width,$height);
    }

    function orientation($file_path){
        $exif = exif_read_data($file_path);
        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                // Поворот на 180 градусов
                case 3: {
                    $this->image = imagerotate($this->image,180,0);
                    break;
                }
                // Поворот вправо на 90 градусов
                case 6: {
                    $this->image = imagerotate($this->image,-90,0);
                    break;
                }
                // Поворот влево на 90 градусов
                case 8: {
                    $this->image = imagerotate($this->image,90,0);
                    break;
                }
            }
        }
    }

    function resize($width,$height) {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }





}