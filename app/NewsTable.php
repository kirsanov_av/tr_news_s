<?php
namespace NewsUpload;


class NewsTable extends RosnouMysql
{


    private $tableName = 'ros_content'; //TODO перепроверить имя таблицы
    public $tableFields = array(
        //'id' => 0,
        'title' => '',
        'alias' => '',
        'title_alias' => '',
        'introtext' => '',
        'fulltext' => '',
        'state' => 0,
        'sectionid' => 3, //Раздел, в который необходимо добавить новость - НОВОСТЬ
        'mask' => 0,
        'catid' => 0,
        'created' => '0000-00-00 00:00:00', //Дата и время создания записи
        'created_by' => 62, //ИД пользователя
        'created_by_alias' => '',
        'modified' => '0000-00-00 00:00:00', //Дата и время изменения записи
        'modified_by' => 62,
        'checked_out' => 0,
        'checked_out_time' => '0000-00-00 00:00:00', //Время, когда статья автоматически снимется с публикации
        'publish_up' => '0000-00-00 00:00:00', //Публикация
        'publish_down' => '0000-00-00 00:00:00', //Снятие с публикации
        'images' => '',
        'urls' => '',
        'attribs' => '',
        'version' => 1,
        'parentid' => 0,
        'ordering' => 0,
        'metakey' => '',
        'metadesc' => '',
        'access' => 0,
        'hits' => 0,
        'metadata' => ''
    );

    public $monthCategId = array(
        '01' => 44,
        '02' => 45,
        '03' => 25,
        '04' => 47,
        '05' => 24,
        '06' => 48,
        '07' => 44,
        '08' => 50,
        '09' => 62,
        '10' => 41,
        '11' => 42,
        '12' => 55,
    );

    public $id = 0;
    public $title = '';
    public $alias = '';
    public $title_alias = '';
    public $introtext = '';
    public $fulltext = '';
    public $state = 1;
    public $sectionid = 3; //Раздел, в который необходимо добавить новость - НОВОСТЬ
    public $mask = 0;
    public $catid = 0;
    public $created = '0000-00-00 00:00:00'; //Дата и время создания записи
    public $created_by = 62; //ИД пользователя
    public $created_by_alias = '';
    public $modified = '0000-00-00 00:00:00'; //Дата и время изменения записи
    public $modified_by = 0;
    public $checked_out = 0;
    public $checked_out_time = '0000-00-00 00:00:00'; //Время, когда статья автоматически снимется с публикации
    public $publish_up = '0000-00-00 00:00:00'; //Публикация
    public $publish_down = '0000-00-00 00:00:00'; //Снятие с публикации
    public $images = '';
    public $urls = '';
    public $attribs = 'show_title=
 link_titles=
 show_intro=
 show_section=
 link_section=
 show_category=
 link_category=
 show_vote=
 show_author=
 show_create_date=
 show_modify_date=
 show_pdf_icon=
 show_print_icon=
 show_email_icon=
 language=
 keyref=
 readmore=';
    public $version = 1;
    public $parentid = 0;
    public $ordering = 1;
    public $metakey = '';
    public $metadesc = '';
    public $access = 0;
    public $hits = 0;
    public $metadata = 'robots=
 author=';


    public function getTableName(){
        return $this->tableName;
    }

    public function setTableName($tableName){
        $this->tableName = $tableName;
    }

    public function createNew(){

    }

    /**
     * Загружает из GET или POST (переданный ассоциативный массив) данные и заполняет переменные по ключу.
     * @param $request
     */
    public function load($request){
        if (is_array($request) && array_key_exists('NewsTable',$request)){

            foreach ($request['NewsTable'] as $key => $value){
                //TODO Обернуть в try
                $this->$key = $value;

            }
            return true;
        }
        else {
            return false;
        }
    }

    public function insertNews(){
        $query = 'INSERT INTO '. $this->tableName . ' ';
        $columns = '(';
        $values = '(';
        foreach ($this->tableFields as $fieldName => $field){
            $columns .= '`'.$fieldName . '`,';
            $values .= '"' . $this->$fieldName . '",';
        }
        //убираем последнюю запятую и закрываем скобку
        $columns = rtrim(rtrim($columns),',') . ')';
        $values = rtrim(rtrim($values),',') . ')';

        //Формируем окончательный запрос
        $query .= $columns;
        $query .= ' VALUES ';
        $query .= $values;
        return $this->exec($query);
    }
}